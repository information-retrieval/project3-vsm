import json
import random
import os
import single_query

def getRandomKeys(data):
    elementSize = len(data)+1
    result = []
    while len(result) <= 9:
        value = random.randrange(1,elementSize)
        if not (value in result):
            result.append(value)
    return result

def generateRandomQueries(data):
    keys = getRandomKeys(data)
    result = []
    for i in keys:
        result.append(data[str(i)])
    return result, keys

def main():
    pathFile = os.path.abspath(".")
    data = None
    with open(pathFile +'/python/JSON/queries.json') as json_file:
        data = json.load(json_file)
    randomQueries, query_keys = generateRandomQueries(data)
    
    cranqrel = open(pathFile+'/documents/cranqrel')

    #Get the relevance judgements: QUERY | DOCUMENT | VALUE
    rel_j = cranqrel.readlines()

    relevance_dict = {}
    for line in rel_j:
        relevance = line.replace("\n", "").split(" ")
        if relevance[0] not in relevance_dict:
            relevance_dict.update({relevance[0]:[{'doc':relevance[1], 'relevance':relevance[2]}]})
        elif relevance[0] in relevance_dict:
            relevance_dict[relevance[0]].append({'doc':relevance[1], 'relevance':relevance[2]})
    

    #Create the frequency dictionary.
    freq_dict, _ = single_query.createFrequencyDictionary()
    weight_dict = single_query.computeTermIDF(freq_dict)

    system_avg_precision = []

    for i in range(len(randomQueries)):
        #Determine the weights for each query.
        query_weights = single_query.processQuery(randomQueries[i]['content'])
        docs = single_query.vectorSpaceModel(query_weights, weight_dict)
        
        #This number is to be matched with the cranqrel document number.
        real_query_no = query_keys[i]

        #print(relevance_dict)
        #docs is a list with key:value pairs of d
        #For each dictionary of documents retrieved in the list 'docs'.
        no_retrieved_documents = len(docs)
        list_of_relevant_documents = relevance_dict[str(real_query_no)]
    
        precision = []
        recall = []
        counter = 0
        positions = []
        for j in range(no_retrieved_documents):
            
            no_relevant_documents = 0

            if str(real_query_no) in relevance_dict:
                #Total relevant documents for this query.
                no_relevant_documents = len(relevance_dict[str(real_query_no)])

            #Number of the document
            doc_no = list(docs[j].keys())[0]
            
            #k is the rank level (can be seen as the iteration level on each document)
            k = j + 1
            
            #Check if the document is relevant.
            for data in list_of_relevant_documents:
                
                if str(doc_no) == data['doc']:
                    counter+=1
                    recall.append(counter/no_relevant_documents)
                    precision.append(counter/k)
                    positions.append(len(recall)-1)
                else:
                    recall.append(counter/no_relevant_documents)
                    precision.append(0)

        interpolation_precision = []
        
        for i in range(11):
            recall_level = i / 10
            for element in positions:
                if recall_level < recall[element]:
                    interpolation_precision.append(precision[element])
                    break
                elif positions.index(element) + 1 == len(positions):
                    interpolation_precision.append(0)

         
        #Special case for 0 interpolation calculation (no relevant documents found)
        if interpolation_precision == []:
            interpolation_precision = [0 for i in range(11)]

        #print(interpolation_precision)
        system_avg_precision.append(interpolation_precision)

    plot_vector = []

    for i in range(11):
        p=0
        for j in range(10):
            p+=system_avg_precision[j][i]
        p=p/11
        plot_vector.append(p)

    json_dict = {"result":{"p":plot_vector, "r":[0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]}}
    json_object = json.dumps(json_dict)
    print(json_object)

if __name__ == "__main__":
    main()