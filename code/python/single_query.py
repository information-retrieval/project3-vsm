import posting_creation as processing
import json
import math
from nltk.tokenize import word_tokenize
from file_processing import removeSymbols
import getPR
import sys
import os

idf_dict = {}

def createFrequencyDictionary():

    #Create the posting list, the document dictionary and the universe set.
    postingList, documents = processing.generatePostingList()
    term_count = 0
    complete_dict = {}
    for key in postingList.keys():
        term_list = []
        for number in postingList[key]:
            doc_terms = documents[number]['content'].replace(".", " ")
            doc_terms = doc_terms.split()
            for term in doc_terms:
                if term == key:
                    term_count += 1

            freq_dict = {number: term_count}
            term_list.append(freq_dict)
            term_count = 0 
        complete_dict.update({key:term_list})
        term_list = 0

    return complete_dict, documents

def computeTermIDF(freq_dict):

    n_total_docs = 1400

    for term in freq_dict.keys():
        dft = len(freq_dict[term])
        term_idf = math.log(n_total_docs/dft, 10)
        idf_dict.update({term:term_idf})
    
    for term,doc_list in freq_dict.items():
        for document in doc_list:
            for doc, term_freq in document.items():
                doc_weight = idf_dict[term] * term_freq
                document[doc] = doc_weight

    return freq_dict  

def processQuery(query):

    query = removeSymbols(query)
    query = word_tokenize(query)

    query_weights = {}

    for term in query:
        if term in idf_dict:
            if term not in query_weights:
                query_weights.update({term:idf_dict[term]})
            elif term in query_weights:
                query_weights[term] +=  idf_dict[term]

    return query_weights

def vectorSpaceModel(query_weights, doc_weights):
    for query_term in query_weights:
        ranked_documents = []
        if query_term in doc_weights:
            #Para cada documento...
            for doc_list in doc_weights[query_term]:
                sc_per_doc = 0
                for doc in doc_list.keys():
                    weight = query_weights[query_term] * doc_list[doc]
                    sc_per_doc += weight
                    dict_doc_ranking = {doc:sc_per_doc} 
                    ranked_documents.append(dict_doc_ranking)
    return ranked_documents


def main():
    freq_dict, collection = createFrequencyDictionary()
    weight_dict = computeTermIDF(freq_dict)
    query = sys.argv[1]
    query_weights = processQuery(query)
    docs = vectorSpaceModel(query_weights, weight_dict)

    complete_str = "{\n"
    for i in range(len(docs)):

        doc_no = list(docs[i].keys())[0]
        doc_w = list(docs[i].values())[0]

        result_str = "\"d{0}\": ".format(i+1) + "{\n"
        result_str += "\"Title\": \"{0}\",\n".format(collection[doc_no]['title'].replace("\n", " "))
        result_str += "\"Authors\": \"{0}\",\n".format(collection[doc_no]['authors'].replace("\n", " "))
        result_str += "\"Content\": \"{0}\",\n".format(collection[doc_no]['content'].replace("\n", " "))
        result_str += "\"Number\": \"{0}\",\n".format(doc_no)
        result_str += "\"Weight\": \"{0}\"\n".format(doc_w)

        if i+1 == len(docs):
            result_str += "}\n"
        else:
            result_str += "},\n" 

        complete_str += result_str

    complete_str += "}\n"
    
    print(json.dumps(complete_str))

if __name__ == "__main__":
    main()
