"""Generate the postingList using the file_processing functions"""

import file_processing as processing
import os

def generatePostingList():
    pathFile = os.path.abspath(".")
    with open(pathFile+'/documents/cran.all.1400') as f:
        documents = f.read().split('\n.I')
    postingList, documents = processing.makePostingList(documents)
    
    return postingList, documents