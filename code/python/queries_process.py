import json
import os

pathFile = os.path.abspath(".")

def process(document):
    document = document.split('\n.W\n')
    I= int(document[0].replace('.I','').replace(' ',''))
    W = document[1].replace('\n', " ")
    return {'content':W, 'number':I}

def makeDocDic(documents):
    dictionary = {(i+1):process(document) for i,document in enumerate(documents)}
    return dictionary

def generateQueries():
    with open(pathFile+'/documents/cran.qry') as f:
        documents = f.read().split('\n.I')
        x = makeDocDic(documents)
        return x
def main():
    data = generateQueries()
    with open(pathFile+'/python/JSON/queries.json', 'w') as outfile:
        json.dump(data, outfile)

if __name__ == "__main__":
    main()