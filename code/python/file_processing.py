"""Importing the Natural Language Tool Kit"""
"""You need to install nltk with pip install nltk
as we are using python 3.7.4 you need to use pip3 install nltk
also for use corpus and tokenize tou need to run in the python shell
the following commands 
import nltk
nltk.download(‘all’)"""
import nltk 
from nltk.corpus import stopwords
from nltk.tokenize import sent_tokenize, word_tokenize

"""Definition of the function process which
returns a dictionary with the information of 
a document""" 
def process(document):
    document = document.split('\n.T\n')[1]
    T, _, document = document.partition('\n.A\n')
    A, _, document = document.partition('\n.B\n')
    B, _, W = document.partition('\n.W\n')
    return {'title':T, 'authors':A, 'biblio':B, 'content':W}

"""Create dictionary with the number of the document
as his key related with the document information for
that we are using a comprehesion list"""
def makeDocDic(documents):
    dictionary = {(i+1):process(document) for i,document in enumerate(documents)}
    return dictionary
#removeSymboles removes [",", "'", "(", ")", "/","."]
def removeSymbols(text):
    symbols = [",", "'", "(", ")", "/"]
    return text.translate({ord(i): '' for i in symbols})
#removeStops it removes the stopwords from english
def removeStops(tokens):
    stops = set(stopwords.words('english'))
    for i in stops:
        if i in tokens:
            del tokens[i]
    if "." in tokens:
        del tokens["."]
    if "-" in tokens:
        del tokens["-"]
    return tokens

"""The function getTokens make a dictionary with all the
words and numbers that appears in all the documents
we remove the following symbols [",", "'", "(", ")", "/","."]
and the stopswords"""
def getTokens(documents):
    data = makeDocDic(documents)
    my_string = ""
    for i in range(len(data)):
        document = data[i+1]
        my_string += removeSymbols(document['content']) +" "
    tokens = removeStops(dict.fromkeys(word_tokenize(my_string),[]))
    return tokens
"""We generate the posting list with the auxiliar functions that we made before"""
def makePostingList(documents):
    index = getTokens(documents)
    dataDoc = makeDocDic(documents)
    for i in range (len(dataDoc)):
        document = dataDoc[i+1]
        content = removeSymbols(document['content'])
        words = list (removeStops(dict.fromkeys(word_tokenize(content))))
        for j in words:
            if j in index:
                if len(index[j]) == 0:
                    index[j] = [i+1]
                else:
                    index[j].append(i+1)
    return index, dataDoc

