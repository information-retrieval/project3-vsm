var {PythonShell} = require('python-shell');
var fs = require('fs');
var results;
var queries;
/* GET home page. */
fs.readFile('./python/JSON/queries.json', 'utf8', function (err, data) {
  if (err) throw err;
  queries = JSON.parse(data);
});
exports.getHomePage = (req, res, next) => {
  res.render('index', { title: 'VSM', 'queries': queries});
};

exports.postQuery = (req, res, next) =>{
  
  var query = req.body.query;
  res.redirect('/search?data=' + encodeURIComponent(query));
};

exports.getSearch = (req, res, next) =>{
  const query = req.query.data
  if(query == undefined){
    res.render('results', { title: 'Search','queries': queries});
  }else{
    const options = {
      mode: 'json',
      encoding: 'utf8',
      pythonOptions: ['-u'],
      scriptPath: './python/',
      args:[query]
    };
    
    var test = new PythonShell('single_query.py', options);
    test.on('message', function (message){
      message = JSON.parse(message)
      res.render('results', { title: 'Search', 'contenido':message, 'query':query, 'queries': queries});
    });
    test.end(function (err,code,signal) {
      if (err) throw err;
    });
  };
};

exports.getClean = (req, res, next) => {
  res.redirect('/search')
}

exports.getPR = (req, res, next) => {
  const options = {
    mode: 'text',
    encoding: 'utf8',
    pythonOptions: ['-u'],
    scriptPath: './python/',
  };
  
  var test = new PythonShell('getPR.py', options);
  test.on('message', function (message){
    res.render('precisionRecall', { title: 'Precision and Recall', 'queries': queries, 'result': JSON.parse(message)});
  });
  test.end(function (err,code,signal) {
    if (err) throw err;
  });
}
