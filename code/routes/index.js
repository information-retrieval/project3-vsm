var express = require('express');
var router = express.Router();
var main = require('../controllers/main');

router.get('/',main.getHomePage);
router.post('/search', main.postQuery);
router.get('/search', main.getSearch);
router.get('/clean', main.getClean);
router.get("/precisionandrecall", main.getPR);

module.exports = router;
