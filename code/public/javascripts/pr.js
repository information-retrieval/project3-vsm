function makeGraph(data){
    if (data != undefined){
        
        var trace = {
            x: data["result"].r,
            y: data["result"].p,
            mode: 'scatter'
          };
    
        var layout = {
            title:'Precision y recall'
        };
        
        var values = [trace];
        
        Plotly.newPlot('precision', values, layout,{responsive: true});
    
    }
}